const fs = require("fs");
const path = require("path");
const handlebars = require("handlebars");
const _ = require("lodash");

// Read config file and check for errors
let config;
try {
  const configFile = fs.readFileSync(path.join(".", "quest-log-config.json"), "utf-8");
  config = JSON.parse(configFile);
} catch (err) {
  console.error("Error reading quest-log-config.json:", err);
  process.exit(1);
}

const src = path.join(".", config.src);
const emptyPost = fs.readFileSync("partials/blank-post.hbs", "utf-8");

/**
 * Creates the folder structure, compiles the handlebars template `./src/pages/blank-post.hbs`
 * and writes a post markdown file into `./src/YEAR/MONTH/`.
 */
function create(post) {
  console.log("Creating post with title:", post.title);
  try {
    createPostFolders(post);
    const content = renderFromExternalTemplate(emptyPost, post);
    const fullPath = path.join(post.localPath, createPostFileName(post));
    fs.writeFileSync(fullPath, content);
    console.log("New post markdown file created in:\n", fullPath);
  } catch (err) {
    console.error("Error creating post:", err);
  }
}

/**
 * Create the folder structure that the post markdown file will go inside `src/YEAR/MONTH/`.
 */
function createPostFolders(post) {
  const yearFolder = path.join(src, "posts", post.year);
  makeDirIfNotExist(yearFolder);
  const monthFolder = path.join(yearFolder, post.month);
  makeDirIfNotExist(monthFolder);
  post.localPath = monthFolder;
}

/**
 * Create the filename for a post `YYYY-MM-DD-HH.mm.ss-title-of-post.markdown`.
 */
function createPostFileName(post) {
  return `${post.year}-${post.month}-${post.day}-${post.time}-${_.kebabCase(post.title)}.markdown`;
}

/**
 * Takes a Handlebars template and JSON data and returns compiled content.
 */
function renderFromExternalTemplate(template, data) {
  const compiledTemplate = handlebars.compile(template);
  return compiledTemplate(data);
}

/**
 * Checks if a folder exists, and creates it if it does not.
 */
function makeDirIfNotExist(filePath) {
  if (!fs.existsSync(filePath)) {
    fs.mkdirSync(filePath, { recursive: true });
    console.log(`Created new folder: ${filePath}`);
  }
}

module.exports = {
  create,
};
