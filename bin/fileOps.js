const fs = require('fs').promises;
const path = require('path');
const copydir = require('copy-dir');
const { rm } = require('fs').promises;

async function initBuild(config) {
  const src = path.join('.', config.src);
  const dest = path.join('.', config.dest);

  await makeDirIfNotExist(dest);
  await rm(path.join(dest, '*'), { recursive: true, force: true });
}

function copyStaticAssets(src, dest) {
  const assets = ['css', 'images', 'fonts', 'api'];
  assets.forEach((asset) => {
    const source = path.join(src, asset);
    const destination = path.join(dest, asset);
    copydir.sync(source, destination);
  });
  copyFavicons(src, dest);
}

function copyFavicons(src, dest) {
  const favicons = [
    'android-chrome-192x192.png',
    'android-chrome-512x512.png',
    'apple-touch-icon.png',
    'favicon-16x16.png',
    'favicon-32x32.png',
    'favicon.ico',
    'site.webmanifest',
  ];
  favicons.forEach((favicon) => {
    fs.copyFile(path.join(src, favicon), path.join(dest, favicon));
  });
}

async function makeDirIfNotExist(filePath) {
  try {
    await fs.mkdir(filePath, { recursive: true });
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }
}

module.exports = { initBuild, copyStaticAssets, makeDirIfNotExist };
