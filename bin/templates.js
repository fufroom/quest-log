const fs = require('fs');
const handlebars = require('handlebars');
const path = require('path');

function insertPartials(templateName, config, src) {
  let completeTemplate;
  const templatePath = templateName === 'post' ?
    path.join(config.partialsDirectory, templateName + '.hbs') :
    path.join(src, config.templatesDirectory, templateName + '.hbs');

  completeTemplate = fs.readFileSync(templatePath, 'utf8');

  config.partials.forEach((partial) => {
    const partialTemplate = fs.readFileSync(path.join(config.partialsDirectory, partial + '.hbs'), 'utf8');
    const partialTag = `{{>${partial}}}`;
    completeTemplate = completeTemplate.replace(partialTag, partialTemplate);
  });

  return completeTemplate;
}

function renderFromExternalTemplate(template, data) {
  const compiledTemplate = handlebars.compile(template);
  return compiledTemplate(data);
}

module.exports = { insertPartials, renderFromExternalTemplate };
