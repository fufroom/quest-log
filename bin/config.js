const fs = require('fs').promises;
const path = require('path');

async function loadConfig() {
  try {
    const configFile = await fs.readFile(path.join('.', 'quest-log-config.json'), 'utf8');
    const config = JSON.parse(configFile);
    config.generator = 'Quest Log';
    return config;
  } catch (err) {
    console.error('Error loading config:', err);
    process.exit(1);
  }
}

module.exports = { loadConfig };
