const yamlFront = require('yaml-front-matter');
const fs = require('fs');
const path = require('path');
const marked = require('marked');

function collectPostsData() {
  const postsDir = path.join('src', 'posts');
  let posts = [];

  // Read each year and month directory
  const years = fs.readdirSync(postsDir);
  years.forEach(year => {
    const months = fs.readdirSync(path.join(postsDir, year));
    months.forEach(month => {
      const postsInMonth = fs.readdirSync(path.join(postsDir, year, month));
      postsInMonth.forEach(postFile => {
        const postPath = path.join(postsDir, year, month, postFile);
        const postContent = fs.readFileSync(postPath, 'utf8');
        const postMeta = yamlFront.loadFront(postContent);
        postMeta.content = marked(postMeta.__content); // Convert markdown to HTML
        delete postMeta.__content;
        posts.push(postMeta); // Add the post's metadata and HTML content to the list
      });
    });
  });

  // Sort posts by date (newest first)
  posts = posts.sort((a, b) => new Date(b.date) - new Date(a.date));

  return posts;
}

module.exports = { collectPostsData };
