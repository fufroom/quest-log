#!/usr/bin/env node

const { format } = require('date-fns');
const questLogPost = require('../lib/quest-log-post');

const now = new Date();
const args = process.argv.slice(2);
const postTitle = args[0] || 'new post';

const postContent = {
  date: now.toISOString(),
  year: format(now, 'yyyy'),
  month: format(now, 'MM'),
  day: format(now, 'dd'),
  time: format(now, 'HH.mm.ss'),
  title: postTitle,
  categories: [],
  tags: [],
  content: `This is a sample post created with Quest Log!

  ![Sample inline image](/images/sample-image.png)  
  Photo by [Nirzar Pangarkar](https://unsplash.com/@nirzar) on [Unsplash](https://unsplash.com)
`
};

questLogPost.create(postContent);
